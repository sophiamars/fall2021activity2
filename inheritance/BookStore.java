package inheritance;

public class BookStore {
    public static void main(String[] args) {
        Book books[] = new Book[5];

        books[0] = new Book("The Hobbit", "J.R.R. Tolkein");
        books[1] = new ElectronicBook("Wildwood", "Colin Meloy", 600000);
        books[2] = new Book("The Hitchiker's Guide to the Galaxy", "Douglas Adams");
        books[3] = new ElectronicBook("Six of Crows", "Leigh Bardugo", 500500);
        books[4] = new ElectronicBook("Lord of the Rings", "J.R.R. Tolkein", 500000);

        for (Book b : books) {
            System.out.println(b);
        }
    }
}
